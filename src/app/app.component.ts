/**
 * Created by sissoko on 22/02/2017.
 */
import {Component} from "@angular/core";

@Component({
    selector:'my-app',
    template:'<h1>My App with Webpack!!!</h1>'
})
export class AppComponent {
}