/**
 * Created by sissoko on 23/02/2017.
 */
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/main.ts',
    output: {
        path:'/',
        filename:'dist/app.bundle.js'
    },
    module: {
        loaders: [
            {test: /\.ts$/, loader:'ts-loader'}
        ]
    },
    resolve: {
        extensions:['.js', '.ts']
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            __dirname
        ),
        new HtmlWebpackPlugin({
            template:'./src/index.html'
        })
    ]
};